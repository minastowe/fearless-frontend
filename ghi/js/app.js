window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
      throw new Error("Response not ok");
    } else {
      const data = await response.json();

      const conference = data.conferences[0];
      const nameTag = document.querySelector(".card-title");
      nameTag.innerHTML = conference.name;

      const detailUrl = `http://localhost:8000${conference.href}`;
      const detailResponse = await fetch(detailUrl);
      if (detailResponse.ok) {
        const details = await detailResponse.json();
        const imageTag = document.querySelector(".card-img-top");
        imageTag.src = details.conference.location.picture_url;
        console.log(details);
      }
    }
  } catch (error) {
    // Figure out what to do if an error is raised
    console.error("error", error);
  }
});

// function createCard(name, description, pictureUrl) {
//   return `
//       <div class="card">
//         <img src="{pictureUrl}" class="card-img-top">
//         <div class="card-body">
//           <h5 class="card-title">{name}</h5>
//           <p class="card-text">{description}</p>
//         </div>
//       </div>
//     `;
// }

// window.addEventListener("DOMContentLoaded", async () => {
//   const url = "http://localhost:8000/api/conferences/";

//   try {
//     const response = await fetch(url);

//     if (!response.ok) {
//       // Figure out what to do when the response is bad
//       throw new Error("Response not ok");
//     } else {
//       const data = await response.json();

//       //

//       let col = 0;
//       for (let conference of data.conferences) {
//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);

//         if (detailResponse.ok) {
//           const details = await detailResponse.json();
//           const title = details.conference.title;
//           const location = details.conference.location.name;
//           const description = details.conference.description;
//           //const start const end
//           const pictureUrl = details.conference.location.picture_url;
//           const html = createCard(title, location, description, pictureUrl);
//           const column = document.querySelector(`#col-${col}`);
//           column.innerHTML += html;
//           col++;
//           if (col > 2) {
//             col = 0;
//           }
//         }
//       }
//     }
//   } catch (error) {
//     // Figure out what to do if an error is raised
//     console.error("error", error);
//   }
// });

// const imageTag = document.querySelector(".card-img-top");
// imageTag.src = details.conference.location.picture_url;
