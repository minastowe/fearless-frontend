import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();

          setLocations(data.locations);
        }
      }
    
      useEffect(() => {
        fetchData();
      }, []);
    
    const [name, setName] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [start, setStart] = useState('');

    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }

    const [end, setEnd] = useState('');

    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }

    const [description, setDescription] = useState('');

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const [maximumPresentation, setMaximumPresentation] = useState('');

    const handleMaximumPresentationChange = (event) => {
        const value = event.target.value;
        setMaximumPresentation(value);
    }

    const [maximumAttendees, setMaximumAttendees] = useState('');

    const handleMaximumAttendeesChange = (event) => {
        const value = event.target.value;
        setMaximumAttendees(value);
    }

    const [location, setLocation] = useState('');

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.start = start;
        data.end = end;
        data.description = description;
        data.maximumPresentation = maximumPresentation;
        data.maximumAttendees = maximumAttendees;
        data.location = location;
        console.log(data);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
      
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newLocation = await response.json();
          console.log(newLocation);

          setName('');
          setStart('');
          setEnd('');
          setDescription('');
          setMaximumPresentation('');
          setMaximumAttendees('');
          setLocation('');
        }
      }
    // }
    //fetch data, use effect, and return methods
    // }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input 
                    value={name}
                    onChange={handleNameChange} 
                    placeholder="Name" 
                    required type="text" 
                    name="name" 
                    id="name" 
                    className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input 
                    value={start}
                    onChange={handleStartChange} 
                    placeholder="Start" 
                    required type="number" 
                    name="start" 
                    id="start" 
                    className="form-control" />
                <label htmlFor="start">Start</label>
              </div>
              <div className="form-floating mb-3">
                <input 
                    value={end}
                    onChange={handleEndChange} 
                    placeholder="end" 
                    required type="number" 
                    name="end" id="end" 
                    className="form-control"/>
                <label htmlFor="end">End</label>
              </div>
              <div className="form-floating mb-3">
                <input 
                    value={description}
                    onChange={handleDescriptionChange} 
                    placeholder="description" 
                    required type="text" 
                    name="description" id="description" 
                    className="form-control"/>
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input 
                    value={maximumPresentation}
                    onChange={handleMaximumPresentationChange} 
                    placeholder="maximumPresentation" 
                    required type="number" 
                    name="maximumPresentation" id="maximumPresentation" 
                    className="form-control"/>
                <label htmlFor="maximumPresentation">Maximum Presentation</label>
              </div>
              <div className="form-floating mb-3">
                <input 
                    value={maximumAttendees}
                    onChange={handleMaximumAttendeesChange} 
                    placeholder="maximum Attendees" 
                    required type="number" 
                    name="maximumAttendees" id="maximumAttendees" 
                    className="form-control"/>
                <label htmlFor="maximumAttendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <select 
                    value={location}
                    onChange={handleLocationChange} 
                    required name="location" 
                    id="location" 
                    className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option key={location.name} value={location.name}>
                            {location.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }

export default ConferenceForm;